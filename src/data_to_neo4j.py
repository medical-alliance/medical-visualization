import json
from src.to_neo4j import dataToNeo4j
from py2neo import Graph, Node

with open("../file/心血管疾病.json", encoding='utf8') as data_json:
    data_json = json.load(data_json)['result']
    for element in data_json:
        for key, value in element.items():
            if key != '简介':
                if key == '疾病名称':
                    if len(value) > 1:
                        status = dataToNeo4j().query_node("疾病名称", name=value[0])
                        if status == 0:
                            node = Node("疾病名称", name=value[0], other_name=value[1:])
                            dataToNeo4j().neo4j_graph.create(node)
                    else:
                        status = dataToNeo4j().query_node("疾病名称", name=value[0])
                        if status == 0:
                            node = Node("疾病名称", name=value[0])
                            dataToNeo4j().neo4j_graph.create(node)
                else:
                    for children in value:
                        status = dataToNeo4j().query_node(key, children)
                        if status == 0:
                            dataToNeo4j().create_node(key, children)
with open("../file/心血管疾病.json", encoding='utf8') as data_json:
    data_json = json.load(data_json)['result']
    for element in data_json:
        key_word = element['疾病名称'][0]
        for key, value in element.items():
            if key != '疾病名称' and key != '简介':
                for children in value:
                    status = dataToNeo4j().relat_exists("疾病名称", key_word, children, '对应' + key)
                    if status:
                        dataToNeo4j().create_relation_ship('疾病名称', key, [[key_word, children]], '对应' + key)
