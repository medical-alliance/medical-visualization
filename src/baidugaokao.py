import re
from src.to_neo4j import dataToNeo4j
import json


class baidugaokao():
    def __init__(self):
        self.save_path = "../file/百度高考/"
        self.Neo = dataToNeo4j()

    def name(self):
        with open(self.save_path + 'name.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                if self.Neo.query_node("高考专业", name=temp[0]) == 0:
                    self.Neo.create_node("高考专业", temp[0])
                if self.Neo.query_node("本科学位", name=temp[1]) == 0:
                    self.Neo.create_node("本科学位", temp[1])
                if self.Neo.query_node("本科年限", name=temp[2]) == 0:
                    self.Neo.create_node("本科年限", temp[2])
                disease = temp[0]
                status = self.Neo.relat_exists("高考专业", disease, temp[1], '专业对应学位')
                if status:
                    self.Neo.create_relation_ship('高考专业', "本科学位", [[disease, temp[1]]], '专业对应学位')
                status = self.Neo.relat_exists("高考专业", disease, temp[2], '专业对应年限')
                if status:
                    self.Neo.create_relation_ship('高考专业', "本科年限", [[disease, temp[2]]], '专业对应年限')

    def study(self):
        with open(self.save_path + 'study.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    print(element)
                    if self.Neo.query_node("学习科目", name=element) == 0:
                        self.Neo.create_node("学习科目", element)
                    status = self.Neo.relat_exists("高考专业", disease, element, '本科学习科目')
                    if status:
                        self.Neo.create_relation_ship('高考专业', "学习科目", [[disease, element]], '本科学习科目')

    def code(self):
        with open(self.save_path + 'code.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    print(element)
                    if self.Neo.query_node("专业代码", name=element) == 0:
                        self.Neo.create_node("专业代码", element)
                    status = self.Neo.relat_exists("高考专业", disease, element, '本科专业代码')
                    if status:
                        self.Neo.create_relation_ship('高考专业', "专业代码", [[disease, element]], '本科专业代码')

    def impress(self):
        with open(self.save_path + 'impress.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    print(element)
                    if self.Neo.query_node("专业第一印象", name=element) == 0:
                        self.Neo.create_node("专业第一印象", element)
                    status = self.Neo.relat_exists("高考专业", disease, element, '本科专业第一印象')
                    if status:
                        self.Neo.create_relation_ship('高考专业', "专业第一印象", [[disease, element]], '本科专业第一印象')

    def xueke(self):
        with open(self.save_path + 'xueke.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    print(element)
                    if self.Neo.query_node("选考学科", name=element) == 0:
                        self.Neo.create_node("选考学科", element)
                    status = self.Neo.relat_exists("高考专业", disease, element, '专业建议选考学科')
                    if status:
                        self.Neo.create_relation_ship('高考专业', "选考学科", [[disease, element]], '专业建议选考学科')

    def do_what(self):
        with open(self.save_path + 'do_what.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                data = temp[2:]
                include_data = {}
                for i in range(len(data)):
                    include_data['行业细分类' + str(i)] = data[i]
                disease = temp[0]
                name = temp[2:]
                for element in name:
                    if self.Neo.query_node("行业细分类", name=element) == 0:
                        self.Neo.create_node("行业细分类", element)
                    status = self.Neo.relat_exists("高考专业", disease, element, '专业对口就业行业')
                    if status:
                        self.Neo.create_relation_ship('高考专业', "行业细分类", [[disease, element]], '专业对口就业行业')
                disease = temp[1]
                name = temp[2:]
                for element in name:
                    if self.Neo.query_node("行业细分类", name=element) == 0:
                        self.Neo.create_node("行业细分类", element)
                    status = self.Neo.relat_exists("对口就业行业", disease, element, '对口就业行业细分类')
                    if status:
                        self.Neo.create_relation_ship('对口就业行业', "行业细分类", [disease, element], '对口就业行业细分类')
    def school(self):
        with open(self.save_path + 'school.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    print(element)
                    if self.Neo.query_node("学校", name=element) == 0:
                        self.Neo.create_node("学校", element)
                    status = self.Neo.relat_exists("高考专业", disease, element, '本科开设院校')
                    if status:
                        self.Neo.create_relation_ship('高考专业', "学校", [[disease, element]], '本科开设院校')


baidugaokao().school()
